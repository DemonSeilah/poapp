import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../Screen/Home';
const Stack = createStackNavigator();
const MainStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

// for you are using nested stack with drawer stack navigation should be export not export default
export {MainStackNavigator};
