import React, {Component} from 'react';
import {CreateDrawerNavigator} from '@react-navigation/drawer';
import {MainStackNavigator} from './StactNavigation';
import SideBar from '../Components/SideBar';
const Drawer = CreateDrawerNavigator();
const DrawerNavigation = () => {
  return (
    <Drawer.Navigator drawerContent={props => <SideBar {...props} />}>
      <Drawer.Screen
        name="Home"
        options={{
          drawerIcon: config => (
            <Icon size={15} name="home" style={{color: '#a7a7a7'}}></Icon>
          ),
        }}
        component={MainStackNavigator}
      />
    </Drawer.Navigator>
  );
};

export default {HomeScreenRouter};
