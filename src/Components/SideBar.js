import React from 'react';
import {Alert, AppRegistry, Image, StatusBar} from 'react-native';
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content,
  Icon,
} from 'native-base';

export default function Sidebar(navigation) {
  return (
    <Container>
      <Content>
        <Image
          source={{
            uri:
              'https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/drawer-cover.png',
          }}
          style={{
            height: 120,
            width: '100%',
            alignSelf: 'stretch',
            position: 'absolute',
          }}
        />
        <Image
          square
          style={{
            height: 80,
            width: 70,
            position: 'absolute',
            alignSelf: 'center',
            top: 20,
          }}
          source={{
            uri:
              'https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/logo.png',
          }}
        />
        <List
          dataArray={routes}
          contentContainerStyle={{marginTop: 120}}
          renderRow={data => {
            return (
              <ListItem button onPress={() => Alert.alert('test')}>
                <Text>{data}</Text>
              </ListItem>
            );
          }}
        />
      </Content>
    </Container>
  );
}
