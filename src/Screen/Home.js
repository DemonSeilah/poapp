/*
  ########  #  #        #   #       #
  #         #  #  #     #     #   #
  #  #####  #  #    #   #       #
  #      #  #  #      # #     #   #
  ########  #  #        #   #        #
  
  */
import React from 'react';

import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Drawer,
  Item,
  Input,
} from 'native-base';

export default function Home() {
  return (
    <Container>
      <Header
        androidStatusBarColor="#7DB9FF"
        style={{backgroundColor: '#7DB9FF'}}>
        <Left>
          <Button transparent>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>Home</Title>
        </Body>
        <Right />
      </Header>
      <Content>
        <Text>This is Content Section</Text>
        {/* <Item>
            <Icon active name="home" />
            <Input placeholder="Icon Textbox" />
          </Item> */}
      </Content>
    </Container>
  );
}
